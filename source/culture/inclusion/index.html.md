---
layout: markdown_page
title: "Inclusion and Development"
---

## Inclusion and Development

![Our Global Team](/images/team-photo-mexico-summit.jpg){: .illustration}*<small>In January 2017, our team of 150 GitLabbers from around the world!</small>*


### GitLab's Commitment to Employee Inclusion and Development

GitLab recognizes that to build a diversity initiative that resonates for our organization, we need to redefine it in a way that embodies our values and reinforces our global workforce. We also know that many [diversity programs fail](https://hbr.org/2016/07/why-diversity-programs-fail). Rather than focusing on building diversity as a collection of activities, data, and metrics, we're choosing [to build and institutionalize](http://www.russellreynolds.com/en/Insights/thought-leadership/Documents/Diversity%20and%20Inclusion%20GameChangers%20FINAL.PDF) a culture that is inclusive and supports all employees equally to achieve their professional goals. We will refer to this intentional culture curation as inclusion and development (i & d).


### A Snapshot of GitLabber's Identities

We've asked employees to voluntarily share their identities so that we can understand more about the makeup of our organization. While diversity data can help us learn more about the effectiveness of our i & d activities and can act as a measurement of the success of our culture, we won't rely solely on this data to build our teams.

#### GitLab Identity Data

Data below is as of 2017-04-13.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|  
| Total Team Members                        | 153   | 100%        |
| Based in the US                           | 73    | 47.71%      |
| Based in the UK                           | 15    | 9.80%       |
| Based in the Netherlands                  | 8     | 5.23%       |
| Based in Other Countries                  | 57    | 37.25%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|  
| Total Team Members                        | 153   | 100%        |
| Men                                       | 125   | 81.70%      |
| Women                                     | 28    | 18.30%      |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 18    | 100 %       |
| Women in Leadership                       | 2     | 11.11%      |
| Men in Leadership                         | 16    | 88.89%      |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 82    | 100%        |
| Men in Development                        | 74    | 90.24%      |
| Women in Development                      | 8     | 9.76%       |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 73    | 100%        |
| Asian                                     | 5     | 6.85%       |
| Black or African American                 | 2     | 2.74%       |
| Hispanic or Latino                        | 2     | 2.74%       |
| Native Hawaiian or Other Pacific Islander | 1     | 1.37%       |
| White                                     | 51    | 69.86%      |
| Unreported                                | 12    | 16.44%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 20    | 100%        |
| Asian                                     | 2     | 10.00%      |
| White                                     | 15    | 75.00%      |
| Unreported                                | 3     | 15.00%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 14    | 100%        |
| Asian                                     | 1     | 7.14%       |
| Native Hawaiian or Other Pacific Islander | 1     | 7.14%       |
| White                                     | 9     | 64.29%      |
| Unreported                                | 3     | 21.43%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 153   | 100%        |
| Asian                                     | 9     | 5.88%       |
| Black or African American                 | 3     | 1.96%       |
| Hispanic or Latino                        | 4     | 2.61%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.65%       |
| Two or More Races                         | 1     | 0.65%       |
| White                                     | 84    | 54.90%      |
| Unreported                                | 51    | 33.33%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 82    | 100%        |
| Asian                                     | 4     | 4.88%       |
| Black or African American                 | 1     | 1.22%       |
| Hispanic or Latino                        | 2     | 2.44%       |
| Two or More Races                         | 1     | 1.22%       |
| White                                     | 39    | 47.56%      |
| Unreported                                | 35    | 42.68%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 18    | 100%        |
| Asian                                     | 1     | 5.56%       |
| Native Hawaiian or Other Pacific Islander | 1     | 5.56%       |
| White                                     | 10    | 55.56%      |
| Unreported                                | 6     | 33.33%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 153   | 100%        |
| 20-24                                     | 13    | 8.50%       |
| 25-29                                     | 53    | 34.64%      |
| 30-34                                     | 44    | 28.76%      |
| 35-39                                     | 17    | 11.11%      |
| 40-49                                     | 16    | 10.46%      |
| 50-59                                     | 9     | 5.88%       |
| 60+                                       | 0     | 0.00%       |
| Unreported                                | 1     | 0.65%       |

### GitLab's 2017 Inclusion and Development goals

1. Rewrite 100% of vacancy descriptions for inclusive language
1. Publish 4 employee spotlight blogs
1. Publish 4 culture focused, employee written blogs
1. Achieve 100% participation on Grovo unconscious bias training by all GitLabbers
